package com.example.peter.trabajoautonomo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button button;
    TextView r;
    EditText g;
    RadioButton f,c ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);button = (Button)findViewById(R.id.button);
        r = (TextView)findViewById(R.id.r);
        f = (RadioButton)findViewById(R.id.f);
        c = (RadioButton) findViewById(R.id.c);
        g = (EditText) findViewById(R.id.g);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(f.isChecked()){
                    double d=0;
                    double value=Double.parseDouble(g.getText().toString());
                    d=(value-32)/1.8000;

                    r.setText(String.valueOf(d));
                }else if(c.isChecked()){

                    double d=0;
                    double value=Double.parseDouble(g.getText().toString());
                    d=(value*1.8000)+32;
                    r.setText(String.valueOf(d));

                }else{
                    r.setText("no ha seleccionado ninguna opcion");
                }

            }
        });
    }
}
